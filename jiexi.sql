/*
Navicat MySQL Data Transfer

Source Server         : 本地服务器
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : jiexi

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-05-26 17:04:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jiexi_admin
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_admin`;
CREATE TABLE `jiexi_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT '' COMMENT '用户名',
  `password` varchar(80) DEFAULT '' COMMENT '密码',
  `ip` varchar(10) DEFAULT NULL COMMENT '最后登录ip',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `type` int(1) DEFAULT NULL COMMENT '类型 1商家 2店长',
  `exp_time` datetime DEFAULT NULL COMMENT '过期时间',
  `day_number` int(11) DEFAULT '20' COMMENT '每日解析次数',
  `delete` int(11) DEFAULT '1' COMMENT '1存在 2删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jiexi_admin
-- ----------------------------
INSERT INTO `jiexi_admin` VALUES ('1', 'admin', '$2y$10$hV8.6bXrRY8MHG4bcrQ5PeKwR/5OEytAlF17ADTrzNYx4otncplue', '127.0.0.1', '2018-05-26 15:20:41', '2', '2018-03-07 08:00:00', '20', '1');
INSERT INTO `jiexi_admin` VALUES ('7', '416716328', '$2y$10$5xaVeKHF6wKBw6pZauvsae1LIW0FpXPlmlDxO9tC5bxEXQhQldszu', '127.0.0.1', '2018-05-26 14:49:26', null, '2018-07-25 14:36:29', '20', '1');

-- ----------------------------
-- Table structure for jiexi_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_auth_group`;
CREATE TABLE `jiexi_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='-- think_auth_group 用户组表， \r\n-- id：主键， title:用户组中文名称， rules：用户组拥有的规则id， 多个规则","隔开，status 状态：为1正常，为0禁用';

-- ----------------------------
-- Records of jiexi_auth_group
-- ----------------------------
INSERT INTO `jiexi_auth_group` VALUES ('1', '超级管理员', '1', '2,3,4,25,27,7,8,13,11,12,32,10,14,15,16,33,34,5,6,44,45,17,18,22,35,37,42,46,38,43,36,39,40,41');
INSERT INTO `jiexi_auth_group` VALUES ('6', '解析会员', '1', '35,37,42,46,38,43');

-- ----------------------------
-- Table structure for jiexi_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_auth_group_access`;
CREATE TABLE `jiexi_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='-- think_auth_group_access 用户组明细表\r\n-- uid:用户id，group_id：用户组id';

-- ----------------------------
-- Records of jiexi_auth_group_access
-- ----------------------------
INSERT INTO `jiexi_auth_group_access` VALUES ('1', '1');
INSERT INTO `jiexi_auth_group_access` VALUES ('2', '1');
INSERT INTO `jiexi_auth_group_access` VALUES ('3', '1');
INSERT INTO `jiexi_auth_group_access` VALUES ('13', '1');
INSERT INTO `jiexi_auth_group_access` VALUES ('4', '6');
INSERT INTO `jiexi_auth_group_access` VALUES ('5', '6');
INSERT INTO `jiexi_auth_group_access` VALUES ('6', '6');
INSERT INTO `jiexi_auth_group_access` VALUES ('7', '6');

-- ----------------------------
-- Table structure for jiexi_cards
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_cards`;
CREATE TABLE `jiexi_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card` varchar(255) DEFAULT '' COMMENT '卡号',
  `password` varchar(255) DEFAULT '' COMMENT '卡密',
  `status` int(11) DEFAULT '1' COMMENT '使用状态 1未使用 2已使用',
  `use_date` datetime DEFAULT NULL COMMENT '使用时间',
  `create_date` datetime DEFAULT NULL COMMENT '卡密创建时间',
  `time` int(11) DEFAULT '7' COMMENT '卡密有效时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jiexi_cards
-- ----------------------------
INSERT INTO `jiexi_cards` VALUES ('4', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '4214e6b0a20dcabf0b15796c013ccc5a', '2', '2018-05-26 12:59:11', '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('5', 'c0b0e79130ba1160ade69a75d64dab26', '07152d46844bec440b06fa21a638491d', '2', '2018-05-26 13:57:22', '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('6', '315afd040609f90a29eb632757bef9da', '024fe782caa28a89d54cf6e91ef6f0bc', '2', '2018-05-26 13:58:24', '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('7', '6fdbbd09849e0a711ebfc09ce79fb691', '1e5b5b3acd3d79c57510f62b1d290ba0', '1', null, '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('8', '291ce60010a16cec427f7de7fb146fce', 'f42c91e8e9d9f67b504624fd3aa75221', '1', null, '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('9', '2d7351deac136f270d181403ca52e7a3', '2fd38a3be8eedfd272bb7ca530479d3d', '1', null, '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('10', '98d972f040198fca58825da742c8aac9', '4c3cc74e3235aa9f59de76d42c092106', '1', null, '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('11', '8c926b0254be8b1415ce28dbe15ccf6b', '24d22185e757793cc138787c6f5f05be', '1', null, '2018-05-25 21:12:16', '7');
INSERT INTO `jiexi_cards` VALUES ('12', 'c1e0da48363493f66983f5753e10b62f', 'eda6a6e31bd1ccd65cffbdc027a6762f', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('13', '399be0eb549ff13047046d98772f0b54', '58917742e6284111639a96d9a348c112', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('14', '03102eec0d569c143a94f866101a12ed', 'e8b07bec66599c451eced6a1f76d2b6c', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('15', 'a09a0540e5efad5bc8bf8b33c4afaac9', '539fd556c6f3d80470bbf967ed5c621d', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('16', '000d7ee9f3e5c4636c4173ed701a90e8', '6039cda5d45cb764376fa43a3086b48f', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('17', '4338b2acedeebe7c8253a7a0121b0383', '6dbaf689bba76bbdf6553e978f35dd83', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('18', '3ffab31d60583752ca9df46def82c83b', 'a42f91cc655ff5cc3d9a8f941b06a82d', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('19', 'fbbb88b50f58bbbcde678faf0609a673', '43fb67e3c7e8f3f639dcefa26df9dad9', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('20', 'be4597d5dd8fd51d2bec6158a350beb7', '313231e352602fa59cd82cae93266c38', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('21', 'f1d42987531e05cbc296a1fbf7fa6fc2', 'ab1fbe7ed36414c7078ef597e0ad2add', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('22', '23fd237651d63a959fe90c38c6004fc2', 'b20f8978eeac6c6dd83074d2952cbbe2', '1', null, '2018-05-25 21:12:27', '7');
INSERT INTO `jiexi_cards` VALUES ('23', '29592fb1d64b52c6cb02a76ccdc80c08', '356c3266ca918d8e410bc296a29ecbc1', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('24', 'd2d279550cbf3b2a162fd63140e73d80', 'd0771511b79b49cfe019e47bc11510a6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('25', '9211301f1382c670f2e5b75a67ba7bb2', '85a4cc0fdeb0bccfb9f8e6228d33f34a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('26', '8ac82f23a33aa2f2b574d1192d1d9b52', '22335d0e5f89e69840b12228eef7af5b', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('27', '740687440b8d1ae31f91472541237829', 'eb47651dfaf9114416b993df512d31fc', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('28', '70668210d60f5b05c28be6bbbdb9a071', '7bb7dcae0e8d5e70f345f47cc471d704', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('29', '65abc218d3ed9383c0ccc7af0568bebd', '6d7b327bbbfbc2820e393f815a7bbad0', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('30', '953ec1dbd3b8d0632148d6d1a677cb77', '7802487daf68bfa46e701c2b46365f2a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('31', '0d60826fce17cdb3c37912d75ea8bba0', '8c3b6fc7246e0bf14e98f5ceea26fea0', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('32', '34b85d2e4448744e795c9ba514ed183e', '96327784294a8918f21bce386b9cc32f', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('33', '3d774b79c1050c7f33d814e189988461', '256ec6124aa183d109afa023922bc587', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('34', '060ef14585bced1ab3293441c0e099cb', 'e5dcb90a9d3c1ae544d5d3e027f74be1', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('35', '92e267dd226ee034ffc239b8b51947bf', '3a70ba865a3124799e49c7d124f4305c', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('36', '7371353b4bd57ad010c1d9dc6db9d9e8', 'c0d820097b449ecca0fe9de67d45d47e', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('37', '2aa66cc3a4f036292d7121d1196a5eae', '34eee02c55b7ee1ada9fcd16210e65c9', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('38', 'cd042b15e00eb808f9a76ca924e07b3a', '4561b67ace93fe2236cf2d30f03a45e3', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('39', 'ead0b31bb584c74b56833f0c14cf548a', 'd2242f169dc0fc92efd02926b87f76b9', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('40', '8603d1e47b78cea8cae45f3aac0e6879', 'c5fb22774b448ee2a1c2b7d07961ec23', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('41', '0bef769181456df9b33feb2614d729f9', 'b30c17821d3bf780651e2f00a6381a00', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('42', 'e183951ae827a0b79af61a7f8493d2d7', 'f5b71cd3425c923ce2309b21186cf38a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('43', '8e4ac41764220ee341c58e48d2a91ef1', '1ed9d9c2dcf491cd608fbaa41fd181f6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('44', '1dd3e4bf48aa3adfa096aa9b581ccdb3', 'e7214eb849f0df688224b8458542d1ae', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('45', 'da0f1acb7aef3ecfa28be8dc06bb4e20', 'c8d53dce1a3ac83deef15008ba92bbe6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('46', '5af5ab5ed89e69052559c5b6dd9d8354', '8e29d192a1b5322818b910bf8decf0b9', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('47', '65f15af3e29ff1ff7a51be3b75b8614a', '7bb5a85136420cd8938a8541af1fdfd9', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('48', 'caef858d8667aa990b0f1c90695bb91c', '8dc2c95892dceb119d9405f684fbc02a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('49', '66a5d825f523a8b03be6cdd2b913cabb', 'ae6dc8f174e4977bc0fc16068a34fe6f', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('50', 'b55a0e17a9240419d82b079d17c83bd2', '0cd18b552b8107e68a8ab447d69d3a42', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('51', 'd30c4de7a8eb33a78add324632b4e523', 'c051c41182c0a04ceb07ff4af7eabc8e', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('52', 'd8861342992dfe7f8f1c91f1a4323948', '3fdc098c0b94137ed72e8280026c2cd7', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('53', 'bb84631aab7fb4d7fde6cb134690445b', 'bc38222dffa9fc647233ecdea1bb7785', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('54', 'f59537587522310084c58266737485de', '5366811f375c3dff915f78b2ef8b03da', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('55', '5fb27cd282ea37e11adb9215ad69bc36', 'c2ad8f44ddc530d2c97e41931b10e7f2', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('56', '40448f08e5952a773e493e96f43d3d62', '5e3d82f67676b58323c38a0c885192ff', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('57', '631c477f32bee5c9dec4bab00ad364ff', '521c6257947f7488445e0e6d15f4d949', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('58', '54234fce4649fe0c15cae8f0d352f91d', '645c986665b4f9f56863e6de9d84061d', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('59', '90804c39b5a1f43fec50a5c2b1eba0f4', 'c676c585487f815654547288aa34d75a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('60', '803766c9cf7817e7ae6e20b01d7abc8d', '6ae398027d4a79cfefa311353e08ef07', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('61', '857aa6005344212005913eb9a6619dd9', '87a8def85b30bd8db0221e3597739f9a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('62', '4d9f3b5239c799c97d96b764838228b2', '9c86a4e305cbedbd2c2b67bb97543ab3', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('63', 'c771890d2308f8bb4aee7921cd32bc8a', 'bef9d02f6d2384c733e1aca044837fa9', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('64', 'a489fedf7a0eb893c88a22af61129429', '838ec55f6030441a642b8b55e0080c1b', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('65', '3f0dc12939f8cd213b728f9b6748c323', '4a4c9c5b9e39d16be7943c32ea0e5bda', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('66', 'd79d1665810cc016bcb43095b61f818b', '965849f572ecc7528b7202a6663af783', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('67', 'd7ca2557595895c04e379c212a13cc02', 'b90f4a69f36b32b2c6c5e03dea179772', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('68', '46b08c48e7de81f01d440180a05f03b5', '5f41fbab0c640d1cadd52809b8e54b4b', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('69', '270c92d1586a07fa7f1b1bb56afe52c7', '23e7598d8f9187c1d867ae73c7524164', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('70', '4425b13f4f06002164fd27938d40821c', 'aa5b779c1fee99ec33975c190ee53edf', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('71', 'b2b31d27936977b72965558e41c4fd18', '6b09884741208169beb154a360093e4c', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('72', '1a2ea3db747aa161daa7285928830dd9', 'f9fd348e94cea55d89a0f1cf401d1050', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('73', '35f7c19a069e3a661638e564b48d11d8', 'ea8b30fd5c6c88ccefeb69ac4b5365ce', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('74', 'dcfa3b8c959fce42bb1b3400351b8358', 'd783d9a2ae57dadca3d4a1b57bc5f694', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('75', '30297321965a33c45f81a93b64305ad4', '17ab7d3dd1ac671c9ee5bc25f9b151aa', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('76', 'f775dee9aa18b1814ae7059e33899afc', '2e0b5f9abd1c5d2ee9c71dc310c0bd43', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('77', 'ccdf1d1bf08a889362d73c8d5936a79d', 'ee7a3e03ecbd48f016ae3d8589ee375e', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('78', '7e91812821537d9fd61046248cecf5df', '070118668db4db30690fbab95877911d', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('79', 'bb9c9757bb424323301d1aab25f3c5e0', '6e0352d82781db3b39c0898025e47188', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('80', 'e8be503e3d7ee5ac02d1246153d90d09', 'a239481d73b13b841ae4cef975ba5253', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('81', '975a5738eb89b577439028b04376e2c4', '649a0e7e48763e75110adb83e8b762d2', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('82', '257a6271a09b8e09a3ec53192326b08e', '25db38b51b0e535fe9c546ad3a765013', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('83', 'f364b3b16501808b86c7c3db400d22f1', 'e6da20e536750e054bb43d5d11e8e988', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('84', 'df2c98354badb94a8c38725d92ada2b1', 'fca66658a0c657703b8e00250c0537d4', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('85', '5da7de19f5f470b05faa11c1e3dece1a', 'c3deacb563d73c713bbf9cb0ba92ccf6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('86', '84c7cf23789b7f7248dfc7744b4b8bdc', '41634128ebb9928b62b78a35b9fb1288', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('87', '2f1d4d753d373297101aa845ce02ca29', '853a6c9b39233262121e2b4d68fd9262', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('88', '8fc7b7f636cd870bd4fd24fd90f3dacc', '2d002d3601bb8e25f8b6fc26c1a2bdca', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('89', 'e2bd4199a9828785082aca25c71508ed', '8d7b54589b8e1dd7170593f870f5fb31', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('90', '61db4f55b3caf289e9ad057617936ed8', 'ad6b3d34905e25cc56e95803578f6bf3', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('91', '5b07326c269ad75e59d80d70fe2b7038', '81a1c5647abc856281fbc4b48d29fb64', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('92', '9a0a7030da2688064921daff5f268287', '2edaaa8f3620668a9436b9ada6141c74', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('93', 'b383c34deb7a09dd2d6aaaeaf67a6431', '885367e3a8a5b413ded5b1951cffcbed', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('94', '1a7e06e8c5b651293eee802ac86872da', '1fe5440ce59641c14b7d673ce7f6cca2', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('95', 'dd0aafb9d6902f09412eedfc08277f98', '0309c8d7f9bf02b23dc5b0131610d8a6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('96', '89a5800612d88217a9d875fc3f4160c2', 'db144f683b515d35879c34872dd6ab1a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('97', '24373ebb04b1cb2486d32c118c185893', 'ee0cb014b3c8588870feb418240c1fbd', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('98', 'bdeb6dccb00260bc7f700ccedbeab1d1', 'cffa315221e0453f9d634e172f350e58', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('99', 'dbe3c97ef9e0836842789239f752da7e', '752895d1f9f72cabce30bc21ff96ea12', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('100', '17363a6436da2bd916878c1419ce6a5b', '241fbcf15e175c63552e6053573ffe8e', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('101', '969485f1fe13babcc1bf0408443d2526', '7dfbbee012f92ecce0743a087d774caf', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('102', '153731b82ad1814f5c0672d72f1223ff', 'e3da573af32e8acb62e37883901d3eaf', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('103', 'f1eb7013c336065acea9736ed612b841', '4902e61ff65eb6855692a5ecc2aa3bd5', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('104', '8d7dbfec6a795c79f67f885cbd36c7c6', '8cc5b3ee879433bfccaea68112b374e0', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('105', '6624a14676c6b6ebee2e0a0aac4cd746', 'd69b2e2b36643774047620720634f305', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('106', 'dcc06bc69c322b7c8187cdb6141bdd17', 'db8e86136156e620ce26f86fb96e277e', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('107', '31443f1de9fc6aa6668cdba03cff9369', 'eaded28df3e5d0c2466739b4e144fc6a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('108', '9833e00d0574c8f6b7ecb93f43688e46', '8c3cae4c551b718d56972eff7a7f26c6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('109', 'e25caffd6d6d60ec6c210e906afafe9e', '7271f57cd1b92cdc5c9affea8e34b7bb', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('110', 'c20a3c444a09291e2f07ed4c977917ef', '93622ec40c7552da251efb5d2840d939', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('111', '84eae5c38af857e85c4272a58134bd85', '4a243bfe8ecbaf72b3ac9dbbb8c336f1', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('112', '338e50280c8ff6d0e5a34fab07453db6', '0ff9b9a5def240e05fa1c8ee63788432', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('113', '6d8073807aa696494f2c07e02be8fc05', 'af95a2bbcbe63f1f67933cc884f1c15d', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('114', 'cedd5e46621561ff26165b4914007d9b', 'e17b346fce995f8766f560876cb357c8', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('115', 'f5e818a7ca0581418f45471aad3a25e8', '3acc75658e90df901d38951769b5d04b', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('116', '98e3e0fb5680079b40db0a08b3e2aa54', 'f10eb4a82252dd73f36a56ae55b21d28', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('117', 'bde010bc560fa199f2d4bb6eb44a46ef', '88ee834e28dbe08c7b7574eafa32933a', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('118', 'dec87859de87f08a78793a587a790c74', '8164bab0ad33d22aa120bfafdf486d3d', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('119', '1eb7051fcba6de6e28148d335588c7f9', 'b11bd8f432bc340d68c73cec56df32c6', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('120', '8dc69c0b10948d7c0cf738bcd485654a', '437f77159303991a18f02c2faeee33f0', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('121', 'b76be328b1370f7709038678af8fb9f9', 'd9433fc1243de47f5d38353ae873fec5', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('122', 'f373cee122dae80f91a3117a9407eabe', '60528b24264863a730b5b3abc394cbff', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('123', '3f3008b169bd70808f246ecef4e5334b', 'ca08a6f19e9ab3acacd2c9ebc678c64f', '1', null, '2018-05-25 21:14:32', '90');
INSERT INTO `jiexi_cards` VALUES ('124', '5bca374af05c223cc7b440cb6f723d2b78dcb342', '3a6e6147221e11b13a03fea19b39ceb00946a525', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('125', '5e346ea2fcaf54c23a89e92e0a8257922656dec8', 'ea687a4665289495152ce9ad768ce610e41c3da1', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('126', '4e5b68b44003b3592bd0947ba94a48ec36ed47e5', '6b170dcdfdf102b9c3c978f72e697294fcaa2658', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('127', '2f3a1051de21dabe3c1b9465b929cbe441eda5c3', 'b32f1ad279f5f7f88ad55d58ead3936e3d223c16', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('128', '41a15e69fd2f1dd86b72c04016f4e3dda2d38dc9', '3ef17b8190619d589a9d515b8bc59decbec81344', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('129', 'd69f423e4857f1f38f299081a1020a0932cd8168', '367f573dede5cbd6ab1c8e147db0f1bda8731e45', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('130', '1dbb3e9f9a3e9aa323cefbb97949b14b9309d916', '2af60d60fd07b14f98d4dc0da19a73bda1632484', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('131', '5475c868e4fac411fc852df4c7ed40734cc8a3da', 'af0467e6157c11f2625dcafb97b2e6de0d7d8f8f', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('132', '47b6e672f39a05b45eca634f48dd869ea4b6fa36', '43660533824b54f298de6d184c70ad7a238f886f', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('133', '9c6037208ad87c4578a069159c18fb00f5d53e55', '788852a0257c29bdc3d4e7a14ae29289b701f6d0', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('134', '6d8ac01e41a4e70b03eff888d0ef4738426ccdbe', 'b2e860829e3dfa515505c427a7aa6c0d21f7d1f6', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('135', 'dd84f64632f85353ce8f8ea32967de36f5b6b94a', '63ae0969444b707da4260b6b548e9be0c2f060c0', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('136', 'b94bd3309fe42e66d7e95562d38c83e46e83962d', '072a2f3e1dd574feedb91492c9bc02121fa3da15', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('137', 'b5a4b6bd6d1a8c58a515dbe03e237e74e8c03cf3', '88bdf85df6625da1b4d1c6e984229246e9b18867', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('138', '5d2a2459009a6a47d6f7b6601681fa93a3c74636', 'e2efe03bd863fed72280639e0883b51a3f9819fe', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('139', '51518d1a83ae62c8a3d5e7f6ee762517851f4de0', 'f4fdedffa71b2a8ae4ecc651df4c3cce3a4e3e4a', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('140', 'bb495c32fcbddb01c77b0bed10f1354c9f101723', 'af0cbeb6bae306d05c396d51ea07bb0a7f7e1a2a', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('141', 'ba7c96f64b9f4ff60a15563892b39be15c9bc3cf', '5159c898f0ce55cdb9680637fe25878bde30ec2d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('142', '78400ed0bea611d696e4b9bf1222514ebcebf1ce', 'a19175dd4d280d927484be18078d0897cc8f355d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('143', '6953a98f570097767ebf5f879ae0a54e64eb10a2', '8dacac9de28046e70917a95d0617d77ecc396bb6', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('144', 'a01f26e28729dfe7dea02c8e41eaf25768c6e3bb', '59345125b62fe65b74729eae42d57ed82fd645ac', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('145', 'e69d39f7f4ddcd6b7721a834f03ac1cae13fe19b', '49a0ead1e89c77a48f34f6484b08d65e84c555b9', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('146', 'c5b8213d0e17bd0bb7621a96531fba300db42fc2', '92b3641440b43b94673f4f828c1ea4cda09a7358', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('147', '55ee62e5f2b295856c7d165906d7d0f0d72a7869', '1bc48be96c345ab0efbe5d752a11fb64cd041b9d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('148', 'd73d327d383c8ee05fdf5e24448a805bd6afa787', 'eda19cac0f334ec6cedfce5ee376095ee891dc1c', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('149', '9fff4d9c8511bd715bb0995d1f110d87544ab763', 'feaebc1a3960e15d5f2af3c4de293d8555f0dae4', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('150', 'da6611a6e682b5af0039ec8dce6024d97776da4e', '00305288bca82874612e4403b83a0b46b9067281', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('151', '7300c9c589e5ce01ab8563717a23e851bf1a2a75', 'e2106b9f8c73fbe348895ed21353ec3c94b3245d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('152', '07bb0ffdba3ea475502ca3f2e68d3333bd0cf82a', '9ea8b8674bbe00a06cd5d4b576e67d69a76efb48', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('153', '59aa4ebe3bf8bd28edbf36c6bab0363680f681c0', '668cf2460944bf68d4f821262595bca74490d1eb', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('154', '236d7037796c7da3950888e1fc8df6b189085f3e', '16a074728cd3dafcec5601ca7f53ca4be56c9a9e', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('155', '36e578de7f7c4b874614fa143a4f76b9c7fa75f5', '79449edbb7d223a95071fd51e4741e5585990c19', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('156', '90e2fba0dd51578ba40a9a55307115ac63af756e', 'b1708f9e6862de64b5777ecf6de4941ed72453a6', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('157', 'cabf1aa43d82eee3e4e21dc62b11099279eedff3', 'ceb9a0a3f2b0b02da94a726ce01ce9c336fdd8c8', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('158', '865c93e61260ad3db9fd99e126551d3aae1dd6b2', '85a1f6d33beac8a0b6ed53446c9b1e24afe61fc7', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('159', 'daccfbfce5afaccb8ef6789decb1cfca139a1a63', '6e4acb149b764bbbe5510468efb803214ab6b1d5', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('160', '920e7195378c848e716ce0b940fe5855c351a3e9', '81f5d75ba49ae44f97e239db2ed5b976aea0b469', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('161', '5ee31f57aed2cee830de66243fcbe8d85fe45533', '30d371576b54b914f7363f1cf42c8961c18eba3d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('162', 'ff77e9c1a7c0d8607662966d84e0668d0a1fcc56', '025edba1dfdfe100ec6f54bb009ddf4f7b0db167', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('163', 'ac051c6fdffa5d9d104aacf027ac2800dbb269bd', 'fb85f56c6287e1e2aeb92a693cecd3d40e0ea287', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('164', 'a087bcc73aa6d8460692013df4233d689b777d87', '4c588e22ca86ff4d8218ea337b44c855683e3e69', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('165', '3a46c73439eee19681a09a2e3329936ce45c803d', 'b4e95fbf3a3f91599be52c97ffedb9186795a450', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('166', '0d01eb6bf326efe0b7cd29059389c6c5f880be96', '9306ae16c0f7b951df90b8fafa7cf7a77fd37a34', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('167', '858c1ae8acf8c73bc4ea762b3bf4d94d2adde8f6', '9a07c5ffb34fe5f2568186dae1ae61a168781d08', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('168', 'f5b124f297e5f5ad9b82ab951acdf1ab53b8a309', '635fb97470ec865a7c83093ccdc7ff4291e2d319', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('169', 'fd2e85bb5ed62b9b3da137957e4fb6342b3ababd', 'ab7af5d3e175109049b8f38740e4fc46b75b3f50', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('170', 'f59a534b4a966e1f3749f1681ec4404a225386e0', '7aef631974be337300766e2a67e8f5b18a9fd9b3', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('171', '0e3d744e8744f094a9f9852e5f0f393ee0632340', 'f5964f9ac38a55a7a8cba9457b416475d551b37c', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('172', '1611c36f5f980a58e2e2ab31995de83a520342f2', '7eac69b934710291a8f3a00f7e8a28f895466b8a', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('173', '73368bf4a9bacb45dbbb989c4b4646290dcd12fb', '66e1bea1ea0fb1b77ec8c14804f2430b4d72b0f8', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('174', 'f3ef8de18638283f3873446b1b09efedd608bbc7', '2dfb42a66665f1a15d57b77cff5e3dcf3fcaa120', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('175', '9dc73224411f26d0967eb6cc328a5fbac2b7d89b', '94da63eb9dcef9f96efa11741d2097b0b4f924a6', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('176', 'f8e2019f6d179a49c2f56616d293de748690e3dc', 'ac0d3778fc8dace021f14acceb8568174ce87251', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('177', 'ff52d9e20c7e2def4ece6373976e321144dd9379', 'eb0f11f925d4cc0e0cf4d42c199b17bb4900edde', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('178', '85ed4ed9ca73fac431b1e8d59f4c6a49153d8278', 'b89e99e98d65b2ced98d964c2cd5b6ac8e3c54ce', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('179', 'b7963814de0ce03a7e778b5feaa1834346209e00', '73d7ea3bb5961671c3dc6d90f2b7e9f5def525b7', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('180', 'b3f08410a6715c56a589b93bc8a97db441e3d12b', '64c8ff69c1b7ca48371b7e1b97035d92d7a6fc28', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('181', 'f05256ca6726e2928cb8e45a74f4a9b0da44b862', '865e15ae062479699774f9bc310fd9e1460672a5', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('182', '7fda00db79aee180bfc2c9ca9344a59bac64f7c8', '3135bc1c39f938eefef6f92d39ae06b8748a44ba', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('183', '56f48d6772d1592bbf740bf021c372411457b1d0', '95174948664d9b04b4a8a7aac5fb0abb1c2d3f2c', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('184', 'e415301f19e3aefcd8c30cf27895bcc2830ad6e2', '960cfd584acbbe5d77fdb0042cff93e1b8fe5d18', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('185', '19e01159be572529b0a15b331315795cfed93e1a', '31e00cbc2cda1a89415bfe8c84e20c12f67bf95d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('186', '6b3aa03c4b18b0b2690ff84b174ed063c6eed6e2', '8258b778285aca17de2c9c96d29cf3d3fc213e47', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('187', '6a070382a4f12437ec23f2331d6ebe3874951645', '64d25abddf4d5e35032ad56ee03a69cbed9d1faf', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('188', '9cf6b935403ec1447fe8e00df5ba3713f75273eb', '58e2473192bdf3c4aff2a87692355e4add31a3c1', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('189', '6d55c38b8ae0bc33eec1c2b282e13530270fc76f', '8df1a26456a58a6104133039eaf5c019d4274c82', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('190', 'e261f59203cf2f61ec13e83b96fb6306ef9aa1f3', 'b2bf1567bf1b03cfd7c7aa516d316e42bf6f6931', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('191', 'a3cd279267cdbc424e29dde3710c5bd9ef66be02', '5bcd77fd69b947304329337edb65d3caf187cc55', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('192', '2b5a6b3f5ca689ac9c2a8ba599b93daf159c0f87', 'caf20a732074b13fb2d607505085cd2058e29cd3', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('193', '300f1471aa27cb9446732c8ebdf6a54a8ef535e2', 'c16d9436f423d96ecc89bf909a72c08551749e17', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('194', 'aa70aa133b49a02b6752b621a094f6ee7b058dec', 'f81cc62069a507e70a6fbcf4ae996a550dc72aa2', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('195', '34c1b5521c8ae867a941e5d32f7280847613c295', '6af0d38ea29725627efeaf4f61e296e18e3938a1', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('196', 'f0133f68d2e3542d29542fae76d1cf7f6a98a48b', '1c1f9284ae558be61c3c3a040c676e4ecc1d3104', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('197', '031c790af1762e0ce39bb2efb495ee91189b2fda', 'fcc67c91baba804b4c42cbc0afb6142a049d39fa', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('198', '74caefb8306b0aceca5796a61e4cdd44ab6aa2a8', 'c475a6c348bdc5ceb2b284315fbdecbb46ec2c05', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('199', 'b724fc65dd461b611281d32a706b795c615e6555', 'b0c5d2e6531d98db1f607edf71bab974141106e7', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('200', 'e70dbc26d93799b4f1a304532ad57533026711ca', 'e299edc7cdc1d19b26f653bc47dfbbe4a333fade', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('201', '2be47f3d601484a6b43336df5e4a13fa2ca5acde', '1ee7001811bcf6bcdedfa3775e0e2f5679f87655', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('202', '754f40dbf1517d45aa3de54a30ab126daef68962', 'cd3da3d413f0d57635326931a9482e20361fd17b', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('203', 'eb41afb961ea2f003df3f67c87dc28dd7099388f', 'df0523ae4f39f5a6021da5677590f13e07cf30e2', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('204', 'c91585393f0d4c7ab29da4d651630925cd3ec1be', '5affa709056412b58d7aa309fbea67d2ac23bcb2', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('205', '73b18f12596b443eb1e896f69c9e83a4bf652825', '98a07baa36d385960715aa8e60f4a16fb732221d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('206', 'ce9dee2ad711cba56e7101ef79de2b00d5a860a1', '568a06192fd1b249526052680b884a43cea9f6b4', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('207', '480c83633a200cd8536f147074961883a5db4ac3', '009c99ca077bb618a08cf584399d93b7e4f72826', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('208', '8902515bbdbe3d456cd5fda4a3d451f3612c8b16', '6c489fadd7b7defa1b99549fa08d04a665806457', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('209', '4b96fa5e72ca83f635a6220eb5c8dbb07247dfe7', '71dd4637d720d0da216c63aff61e55788828c454', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('210', '863c9a30e5075725c3f508b490a53f7734e0307b', '2c6544baff1bd5f5e5d739d76e61130526bd49e4', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('211', 'cfc46ccf25484f93e61d4509ff150c4f6433612a', '8455acab0741361ba6e3140f28523b8a8069eee2', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('212', '758df9690205403a129709e76ad2eb276f23c0ab', 'd03940e1819c28d5fdd262e6c925c96ae4e3184b', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('213', 'a1aafc0fb6f5e3d9c454282ac923171ffce5aec7', '0ae8c08389a51dd30a35434891e1c305f3cce38a', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('214', 'c4044dbcd2743fb1e2c662ec8fa37c4278dbf79f', '40614e8da926cc9e0d94385f41aa7e55ce31d79c', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('215', '6202ae869ba1a468702f45f91617eb1f5c998616', '4c116981692519347abe277a0250e62f125bad95', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('216', '71403cba2bb3db7fbfe519225007cb78872a4dc2', '80f7421fb1862445ec04244276e882f793ecd79b', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('217', '235827877121b4d63d8752058a76b54aae9bddf6', 'ed8291adfbb8c6310adbcc5437654a395f6cd8dd', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('218', '20f6815962e8f5153fd4f4eabf35fea78ce1d677', '4bd1421436fe18db85a8c0907eea7c17de9222e9', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('219', 'd955f1cc1932837c6cc3b694e1bcb72945bf39ea', 'efa002960213d9758a72f4a7768344c8a37164c3', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('220', '7902d4cefe7fa063c06861ecc9af9dda88f90db9', 'b796856b22c24ad87c6779ca1d9f0d7e73f73c4d', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('221', '24b7cf0eca672d2487c5a541fad3ee107727f130', 'a9719b88442511fa9662f3b0a35238f07b308876', '1', null, '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('222', '9738969c3d44de6720d0fc010e63f8f7670e610e', '5e655606688f69bc05370df2777a614a648a0118', '2', '2018-05-26 14:51:39', '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('223', '0bca4d308f1853c8d76755074138487900216f41', '47d19d75644671e963e65baa156595795eeeab15', '2', '2018-05-26 14:41:27', '2018-05-25 21:30:07', '30');
INSERT INTO `jiexi_cards` VALUES ('224', '5b34347e5a8da116eab2e84603d793f4db14b7e7', '635902eab5a36ac8a691e83c9d148d395e672cc0', '2', '2018-05-26 14:00:36', '2018-05-25 21:30:07', '30');

-- ----------------------------
-- Table structure for jiexi_jiexi_log
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_jiexi_log`;
CREATE TABLE `jiexi_jiexi_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `before_url` varchar(200) DEFAULT NULL COMMENT '原来的资料哦地址',
  `after_url` varchar(200) DEFAULT NULL COMMENT '解析后的资料地址',
  `created_time` datetime DEFAULT NULL COMMENT '资料解析时间',
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jiexi_jiexi_log
-- ----------------------------
INSERT INTO `jiexi_jiexi_log` VALUES ('4', 'https://wenku.baidu.com/view/fbc1ac98f424ccbff121dd36a32d7375a517c649.html', 'https://wkbos.bdimg.com/v1/wenku91//a5a70ad517529ce027e3a1f0ed9a260c?responseContentDisposition=attachment%3B%20filename%3D%22%25E5%258F%25B0%25E5%25BC%258F%25E7%2594%25B5%25E8%2584%2591%25E4%25BC%25B', '2018-05-26 01:21:04', '1');
INSERT INTO `jiexi_jiexi_log` VALUES ('5', 'https://wenku.baidu.com/view/fbc1ac98f424ccbff121dd36a32d7375a517c649.html', 'https://wkbos.bdimg.com/v1/wenku91//a5a70ad517529ce027e3a1f0ed9a260c?responseContentDisposition=attachment%3B%20filename%3D%22%25E5%258F%25B0%25E5%25BC%258F%25E7%2594%25B5%25E8%2584%2591%25E4%25BC%25B', '2018-05-26 11:44:23', '1');
INSERT INTO `jiexi_jiexi_log` VALUES ('6', 'https://wenku.baidu.com/view/fbc1ac98f424ccbff121dd36a32d7375a517c649.html', 'https://wkbos.bdimg.com/v1/wenku91//a5a70ad517529ce027e3a1f0ed9a260c?responseContentDisposition=attachment%3B%20filename%3D%22%25E5%258F%25B0%25E5%25BC%258F%25E7%2594%25B5%25E8%2584%2591%25E4%25BC%25B', '2018-05-26 13:52:43', '1');
INSERT INTO `jiexi_jiexi_log` VALUES ('7', 'https://wenku.baidu.com/view/2fb9a017657d27284b73f242336c1eb91a373323.html', 'https://wkbos.bdimg.com/v1/wenku91//e53db37fcc31a4993e13c92ff06926dc?responseContentDisposition=attachment%3B%20filename%3D%22jsp%25E8%25AF%2595%25E9%25A2%2598.doc%22%3B%20filename%2A%3Dutf-8%27%27jsp', '2018-05-26 14:50:45', '7');

-- ----------------------------
-- Table structure for jiexi_menus_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_menus_rule`;
CREATE TABLE `jiexi_menus_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(30) DEFAULT NULL,
  `name` char(80) DEFAULT NULL,
  `title` char(20) DEFAULT '',
  `isshow` int(1) DEFAULT '1' COMMENT '是否显示 1显示 2不显示',
  `pid` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `ismenu` int(1) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='-- think_auth_rule，规则表，\r\n-- id:主键，name：规则唯一标识, title：规则中文名称 status 状态：为1正常，为0禁用，condition：规则表达式，为空表示存在就验证，不为空表示按照条件验证';

-- ----------------------------
-- Records of jiexi_menus_rule
-- ----------------------------
INSERT INTO `jiexi_menus_rule` VALUES ('1', 'dsfgds1', 'Index/index', '系统首页', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('2', 'fa-lock', 'Auth/lists', '权限管理', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('3', 'fa-align-center', 'Menus/lists', '菜单管理', '1', '2', '1', '1', '', '1', '0,3');
INSERT INTO `jiexi_menus_rule` VALUES ('4', 'fa-align-center', 'Menus/lists', '菜单列表', '1', '3', '1', '1', '', '1', '0,3,4');
INSERT INTO `jiexi_menus_rule` VALUES ('5', 'fa-users', 'Member/lists', '会员管理', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('6', 'fa-group', 'Member/lists', '会员列表', '1', '5', '1', '1', '', '1', '0,6');
INSERT INTO `jiexi_menus_rule` VALUES ('7', 'fa-shield', 'AuthGroup/lists', '权限组管理', '1', '2', '1', '1', '', '1', '0,7');
INSERT INTO `jiexi_menus_rule` VALUES ('8', 'fa-shield', 'AuthGroup/lists', '权限组列表', '1', '7', '1', '1', '', '1', '0,7,8');
INSERT INTO `jiexi_menus_rule` VALUES ('10', 'fa-align-center', 'Menus/created', '添加菜单', '1', '2', '1', '1', '', '2', '0,3,4');
INSERT INTO `jiexi_menus_rule` VALUES ('11', 'fa-shield', 'AuthGroup/getAuth', '获取权限列表', '1', '7', '1', '1', '', '2', '0,7,11');
INSERT INTO `jiexi_menus_rule` VALUES ('12', 'fa-shield', 'AuthGroup/created', '添加权限组', '1', '7', '1', '1', '', '2', '0,7,12');
INSERT INTO `jiexi_menus_rule` VALUES ('13', 'fa-shield', 'AuthGroup/modify', '修改权限组', '1', '8', '1', '1', '', '2', '0,7,8,13');
INSERT INTO `jiexi_menus_rule` VALUES ('14', 'fa-user-circle-o', 'Admin/lists', '管理员管理', '1', '2', '1', '1', '', '1', '0,14');
INSERT INTO `jiexi_menus_rule` VALUES ('15', 'fa-user-circle-o', 'Admin/lists', '管理员列表', '1', '14', '1', '1', '', '1', '0,14,15');
INSERT INTO `jiexi_menus_rule` VALUES ('16', 'fa-user-circle-o', 'Admin/created', '添加管理员', '1', '14', '1', '1', '', '2', '0,14,16');
INSERT INTO `jiexi_menus_rule` VALUES ('17', 'fa-codepen', 'System/lists', '系统管理', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('18', 'fa-codepen', 'System/lists', '基本配置', '1', '17', '1', '1', '', '1', '0,18');
INSERT INTO `jiexi_menus_rule` VALUES ('22', 'fa-codepen', 'System/created', '添加修改系统配置', '1', '17', '1', '1', '', '2', '0,22');
INSERT INTO `jiexi_menus_rule` VALUES ('25', 'fa-align-center', 'Menus/modify', '修改菜单', '1', '3', '1', '1', '', '2', '0,3,25');
INSERT INTO `jiexi_menus_rule` VALUES ('27', 'fa-align-center', 'Menus/delete', '删除权限菜单', '1', '3', '1', '1', '', '2', '0,3,27');
INSERT INTO `jiexi_menus_rule` VALUES ('32', 'fa-shield', 'AuthGroup/delete', '删除权限组', '1', '7', '1', '1', '', '2', '0,7,32');
INSERT INTO `jiexi_menus_rule` VALUES ('33', 'fa-user-circle-o', 'Admin/modify', '编辑管理员', '1', '14', '1', '1', '', '2', '0,14,33');
INSERT INTO `jiexi_menus_rule` VALUES ('34', 'fa-user-circle-o', 'Admin/delete', '删除管理员', '1', '14', '1', '1', '', '2', '0,14,34');
INSERT INTO `jiexi_menus_rule` VALUES ('35', 'fa-gitlab', 'Jiexi/lists', '解析系统', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('36', 'fa-id-card-o', 'Card/lists', '卡密管理', '1', '0', '1', '1', '', '1', '0');
INSERT INTO `jiexi_menus_rule` VALUES ('37', 'fa-file-word-o', 'Wenku/lists', '百度文库', '1', '35', '1', '1', '', '1', '0,37');
INSERT INTO `jiexi_menus_rule` VALUES ('38', 'fa-user-circle', 'Account/lists', '账号状态', '1', '35', '1', '1', '', '1', '0,38');
INSERT INTO `jiexi_menus_rule` VALUES ('39', 'fa-id-card-o', 'Card/lists', '卡密列表', '1', '36', '1', '1', '', '1', '0,39');
INSERT INTO `jiexi_menus_rule` VALUES ('40', 'fa-id-card-o', 'Card/created', '创建卡密', '1', '36', '1', '1', '', '2', '0,40');
INSERT INTO `jiexi_menus_rule` VALUES ('41', 'fa-id-card-o', 'Card/delete', '删除卡密', '1', '36', '1', '1', '', '2', '0,41');
INSERT INTO `jiexi_menus_rule` VALUES ('42', 'fa-file-excel-o', 'Wenku/jiexi', '文库解析', '1', '37', '1', '1', '', '2', '0,37,42');
INSERT INTO `jiexi_menus_rule` VALUES ('43', 'fa-dollar', 'Account/created', '卡密充值', '1', '38', '1', '1', '', '2', '0,38,43');
INSERT INTO `jiexi_menus_rule` VALUES ('44', 'fa-user-circle-o', 'Member/modify', '修改会员资料', '1', '6', '1', '1', '', '2', '0,6,44');
INSERT INTO `jiexi_menus_rule` VALUES ('45', 'fa-user-circle-o', 'Member/delete', '删除会员', '1', '6', '1', '1', '', '2', '0,6,45');
INSERT INTO `jiexi_menus_rule` VALUES ('46', 'fa-institution', 'Wenku/delete', '删除解析记录', '1', '37', '1', '1', '', '2', '0,37,46');

-- ----------------------------
-- Table structure for jiexi_number_log
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_number_log`;
CREATE TABLE `jiexi_number_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `jiexi_time` int(20) DEFAULT NULL COMMENT '解析时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jiexi_number_log
-- ----------------------------
INSERT INTO `jiexi_number_log` VALUES ('1', '1', '1527305841');
INSERT INTO `jiexi_number_log` VALUES ('2', '1', '1527306263');
INSERT INTO `jiexi_number_log` VALUES ('3', '1', '1527313963');
INSERT INTO `jiexi_number_log` VALUES ('4', '7', '1527317445');

-- ----------------------------
-- Table structure for jiexi_pay_log
-- ----------------------------
DROP TABLE IF EXISTS `jiexi_pay_log`;
CREATE TABLE `jiexi_pay_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_time` datetime DEFAULT NULL COMMENT '充值时间',
  `card` varchar(255) DEFAULT NULL COMMENT '卡号',
  `uid` int(11) DEFAULT NULL,
  `add_day` int(11) DEFAULT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jiexi_pay_log
-- ----------------------------
INSERT INTO `jiexi_pay_log` VALUES ('1', '2018-05-26 12:52:09', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('2', '2018-05-26 12:53:01', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('3', '2018-05-26 12:55:05', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('4', '2018-05-26 12:55:42', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('5', '2018-05-26 12:55:51', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('7', '2018-05-26 12:59:11', 'bf2d8fc7e25d6ad8c9f13a78020099a9', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('8', '2018-05-26 13:57:22', 'c0b0e79130ba1160ade69a75d64dab26', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('9', '2018-05-26 13:58:24', '315afd040609f90a29eb632757bef9da', '1', '7');
INSERT INTO `jiexi_pay_log` VALUES ('10', '2018-05-26 14:00:36', '5b34347e5a8da116eab2e84603d793f4db14b7e7', '1', '30');
INSERT INTO `jiexi_pay_log` VALUES ('11', '2018-05-26 14:41:27', '0bca4d308f1853c8d76755074138487900216f41', '7', '30');
INSERT INTO `jiexi_pay_log` VALUES ('12', '2018-05-26 14:51:39', '9738969c3d44de6720d0fc010e63f8f7670e610e', '7', '30');
