<?php
/**
 * @Author 张超.
 * @Copyright http://www.zhangchao.name
 * @Email 416716328@qq.com
 * @DateTime 2018/5/26 11:45
 * @Desc
 */

namespace app\run\controller;


use app\common\controller\Run;
use app\run\model\Cards;
use app\run\model\PayLog;
use think\Db;

class Account extends Run
{
    public function lists(){
        if ($this->request->isAjax()){
            //根据条件查询数据、之后再进行分页
            if (input("get.password")) {
                $map[] = ['password', "like", "%" . input("get.password") . "%"];
            }
            if (input("get.status")) {
                $map[] = ['status', "eq", input("get.status")];
            }
            if (input("get.card")) {
                $map[] = ['card', "like", "%" . input("get.card") . "%"];
            }
            if (input("get.time")) {
                $map[] = ['time', "eq", input("get.time")];
            }
            $map[] = ['uid', "eq", $this->userInfo['id']];
            //读取菜单列表
            $model = new PayLog();
            $list = $model->where($map)->order("id desc")->select()->toArray();
            //分页
            $result = paging(input("get.page"), input("get.limit"), $list);
            $this->layAjax(200, "数据获取成功！", count($list), $result);
        }else{
            $user = new \app\run\model\Admin();
            $userInfo = $user->where('id','eq',$this->userInfo['id'])->find();
            $this->assign("user",$userInfo);
            return $this->fetch();
        }
    }
    public function created(){
        $user = new \app\run\model\Admin();
        $userInfo = $user->where('id','eq',$this->userInfo['id'])->find();
        if ($this->request->isAjax()){
            $card = new Cards();
            //组装条件
            $map[] = ['card','eq',input("post.card")];
            $map[] = ['password','eq',input("post.password")];
            $result = $card->where($map)->find();
            if (!$result){
                $this->ajaxRuturn(400,"卡号或密码错误！");
            }
            if ($result['status'] == 2){
                $this->ajaxRuturn(400,"此卡已被使用、请更换！");
            }
            Db::startTrans();
            $pay = new PayLog();
            $data['pay_time'] = date('Y-m-d H:i:s');
            $data['card'] = $result['card'];
            $data['uid'] = $this->userInfo['id'];
            $data['add_day'] =  + $result['time'];
            //添加充值记录
            $payResult = $pay->save($data);
            $exp_time = date('Y-m-d H:i:s',strtotime("+{$result['time']} day"));
            //修改当前账户的过期时间
            $saveResult = $user->save(['exp_time'=>$exp_time],['id'=>$this->userInfo['id']]);
            //修改充值卡状态
            $cardResult = $card->save(['status'=>2,'use_date'=>date("Y-m-d H:i:s")],['card'=>input("post.card"),'password'=>input("post.password")]);
            //增加时间
            if ($saveResult && $payResult && $cardResult){
                Db::commit();
                $this->ajaxRuturn(200,"充值成功、退出登录后生效！");
            }else{
                Db::rollback();
                $this->ajaxRuturn(400,"充值失败！");
            }
        }else{
            $this->assign("user",$userInfo);
            return $this->fetch();
        }
    }
}