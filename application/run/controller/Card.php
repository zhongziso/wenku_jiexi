<?php
/**
 * @Author 张超.
 * @Copyright http://www.zhangchao.name
 * @Email 416716328@qq.com
 * @DateTime 2018/5/25 20:35
 * @Desc
 */

namespace app\run\controller;


use app\common\controller\Run;
use app\run\model\Cards;

class Card extends Run
{
    public function lists()
    {
        if ($this->request->isAjax()) {
            //根据条件查询数据、之后再进行分页
            if (input("get.password")) {
                $map[] = ['password', "like", "%" . input("get.password") . "%"];
            }
            if (input("get.status")) {
                $map[] = ['status', "eq", input("get.status")];
            }
            if (input("get.card")) {
                $map[] = ['card', "like", "%" . input("get.card") . "%"];
            }
            if (input("get.time")) {
                $map[] = ['time', "eq", input("get.time")];
            }
            $map[] = ['time', "neq", ''];
            //读取菜单列表
            $model = new Cards();
            $list = $model->where($map)->order("id desc")->select()->toArray();
            foreach ($list as $key => &$val) {
                $val['use_date'] = $val['status'] == 1 ? "此卡尚未使用" : $val['use_date'];
                $val['time'] = $val['time']."天时长";
            }
            //分页
            $result = paging(input("get.page"), input("get.limit"), $list);
            $this->layAjax(200, "数据获取成功！", count($list), $result);
        } else {
            return $this->fetch();
        }
    }

    public function created()
    {
        if ($this->request->isAjax()) {
            $post = input("post.");
            $list = [];
            $create_date = date("Y-m-d H:i:s");
            for ($i = 0; $i <= $post['number']; $i++) {
                if ($post['type'] == 'md5') {
                    $card = md5(createNonceStr());
                    $password = md5(md5(createNonceStr()));
                } else {
                    $card = sha1(createNonceStr());
                    $password = sha1(sha1(createNonceStr()));
                }
                $list[$i]['card'] = $card;
                $list[$i]['password'] = $password;
                $list[$i]['status'] = 1;
                $list[$i]['create_date'] = $create_date;
                $list[$i]['time'] = $post['time'];
            }
            $card = new Cards();
            $result = $card->saveAll($list);
            if ($result) {
                $this->ajaxRuturn(200, "卡密添加成功！");
            } else {
                $this->ajaxRuturn(400, "卡密创建失败！");
            }
        } else {
            return $this->fetch();
        }
    }
    public function delete(){
        if ($this->request->isAjax()){
            $post = input("post.id");
            $ids = explode(",",ltrim($post,","));
            $model = new Cards();
            //组装条件
            $map[] = ['id',"in",$ids];
            $result = $model->where($map)->delete();
            if ($result){
                $this->ajaxRuturn(200,"删除成功！");
            }else{
                $this->ajaxRuturn(400,"删除失败！");
            }
        }
    }
}