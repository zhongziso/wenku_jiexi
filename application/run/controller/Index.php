<?php
namespace app\run\controller;
use app\common\controller\Run;
use think\facade\Session;

class Index extends Run
{
    public function index()
    {
        return $this->fetch("down_page");
    }
    public function down_page(){
        return $this->fetch("index");
    }
}